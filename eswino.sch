EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 2
Title "ESWINO"
Date "20/12/2019"
Rev "v002"
Comp "Startup"
Comment1 "Proto_ATSAMD21G18A"
Comment2 "Development status"
Comment3 ""
Comment4 ""
$EndDescr
$Sheet
S 1150 1000 500  500 
U 5EE620E3
F0 "ATSAMD21G18A" 50
F1 "ATSAMD21G18A.sch" 50
$EndSheet
Wire Notes Line
	2350 2350 2350 500 
Text Notes 850  2250 0    50   ~ 0
Placa Funcional ATSAMD21G18A\n
Wire Notes Line
	4300 2350 4300 500 
Wire Notes Line
	500  2350 4300 2350
$EndSCHEMATC
